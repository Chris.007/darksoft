<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{ 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    //IMPORTANTE SE ESTA USANDO LA MISMA VISTA YA QUE NO HAY DIFERECIA ENTRE ADMI Y DOC/AUX
    
    public function index()//USUARIO NORMAL
    {
        return view('admin-home'); 
    }
    public function adminHome()//ADMINISTRADOR
    {
        return view('admin-home'); 
    }
}
