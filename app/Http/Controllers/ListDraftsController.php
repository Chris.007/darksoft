<?php

namespace App\Http\Controllers;

use App\Models\Reserve;
use Illuminate\Http\Request;
use App\Models\Person;
use App\Models\Subject;
use App\Models\DraftForm;
use App\Models\ReserveDraftCourse;
use App\Models\AssignSubject;

class ListDraftsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   /*
        $person = Person::find(3);
        $drafts = DraftForm::where('id_person',3)->get();
        $subjects = Subject::all();
        $draftGroups =  ReserveDraftCourse::all();
        $assignSubject = AssignSubject::all();

        return view('docenteAuxiliar.listDrafts', compact('person','subjects','drafts','draftGroups','assignSubject'));
        */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        /*
        protected $table = 'draft_form';
        protected $primaryKey = 'id_draft_form';
        protected $foreignPerson = 'id_person';
        protected $foreignMotive = 'id_motive';
        protected $foreignAssignSubject = 'id_assign_subject';

        protected $fillable = [
            'data_reserve_df', 'number_students_df', 'continuous_classroom_df'
        ];
        */
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $reservas = Reserve::where('id_person',$id)->where('enviado', false)->get();
        return view('docenteAuxiliar.listDrafts', ['reservas'=>$reservas]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
