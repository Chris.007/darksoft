<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reserve;
use App\Models\ReserveClassroom;

class ListInformationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {

        // $reservas = Reserve::whereIn('state', ['ACEPTADO', 'RECHAZADO'])
        // ->get();         , ['reservas' => $reservas]
        $listInfo = ReserveClassroom::paginate(6);
        return view('administrador.listInformation', ['listInfo' => $listInfo]);
    }
}
