<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Person;
use App\Models\Subject;
use App\Models\MotiveReserve;
use App\Models\AssignSubject;
use App\Models\AssignClass;
use App\Models\AssignClassroom;
use App\Models\Classroom;
use App\Models\ReserveCourse;
use App\Models\ReserveClassroom;
use App\Models\Reserve;
use App\Models\ClassTime;

class ListReserva extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

        $conRespuesta = ReserveClassroom::select('id_form')->get();
        $reservas = Reserve::where('enviado', true)
        ->whereNotIn('id_form', $conRespuesta)
        ->get();
        return view('administrador.listReserve', ['reservas'=>$reservas]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    //MOSTRAR UNA SOLICITUD
    public function show($formIdR) {
        $formR = Reserve::find($formIdR);
        $detalle = Reserve::where('id_form',$formIdR)->first();
        $reserveCourse = ReserveCourse::where('id_form', $formIdR)->first();
        $assignCurso = AssignSubject::where('id_assign_subject', $reserveCourse->id_assign_subject)->first();
        $user = Person::where('id_person', $assignCurso->id_person)->first();
        $materia = Subject::where('id_subject', $assignCurso->id_subject)->first();
        $motivo = MotiveReserve::where('id_motive', $detalle->id_motive)->first();
        $assign = AssignClass::where('id_form', $formIdR)->get();//Periodo-horario

        $periodos = ClassTime::all();
        $numEst = Reserve::where('id_form',$formIdR)->pluck('number_students')->first();

        //Existe fecha solicitada en la BD
        $listClassroom = Classroom::where('capacity','>=',$numEst)
        ->orderBy('capacity')
        ->get();

        $myfechaSol = $detalle->date_reserve; // 2022/06/16
        $listaFechas = AssignClassroom::where('date_reserve', $myfechaSol)->get();
        $existeFecha = True;

        //AULAS CONTIGUAS
        //filtrar periodo de reserva
        $idHorarios= AssignClass::where('id_form', $formIdR)
        ->select('id_class_timetable')
        ->get();
        //seleccionamos la fecha y el perido buscado
        $unAula = AssignClassroom::where('date_reserve', $detalle->date_reserve)
        ->whereIn('id_class_timetable', $idHorarios)
        ->select('id_classroom')
        ->get();
        //seleccionamos solo aulas continuas disponibles quitando aulas no continuas
        $classroomContinuo = Classroom::where('capacidadac','>=',$numEst)
        ->whereNotIn('idcontinuo', $unAula)
        ->orderBy('capacidadac')
        ->get();


        if($listaFechas->isEmpty()){
            $existeFecha = False;
        }

        return view('administrador.reserveClassroom',
            ['form'=>$formR, 'docente'=>$user, 'detalle'=> $detalle,
            'materia'=> $materia, 'motivo'=> $motivo, 'assignClassroom'=> $assign,
            'periodos'=> $periodos, 'listClassroom' => $listClassroom,
            'existeFecha'=> $existeFecha, 'listaFechas' => $listaFechas,
            'continuo' => $classroomContinuo

        ]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
    public function filter(Request $request)
    {
        $solicitudesRespondidas = ReserveClassroom::select('id_form')->get();
        $reservas = Reserve::where('enviado', true)
            ->whereNotIn('id_form', $solicitudesRespondidas)
            ->where('id_motive', $request->motivo);
        if ($request->fechaReserva) {
            $reservas->whereDate('date_reserve', $request->fechaReserva);
        }
        if ($request->fechaSolicitud) {
            $reservas->whereDate('updated_at', $request->fechaSolicitud);
        }
        return view('administrador.listReserve', ['reservas'=>$reservas->get()]);
    }
}
