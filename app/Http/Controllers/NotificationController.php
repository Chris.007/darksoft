<?php
namespace App\Http\Controllers;
use App\Models\Reserve;
use App\Models\ReserveClassroom;
use Illuminate\Http\Request;
use PHPUnit\Exception;

class NotificationController extends Controller
{
    public function create()
    {
        $solicitud = Reserve::find(19);
        return view('administrador.notification', ['solicitud'=>$solicitud]);
    }
    public function store(Request $request) {
        try {
            $respuesta = new ReserveClassroom();
            $respuesta->id_form = $request->id_solicitud;
            $respuesta->notified = true;
            $respuesta->state_reserve = true;
            $respuesta->comments = $request->cometarios;
            $respuesta->save();
            $notification = array(
                'message' => 'La Notificacion se envio con exito!',
                'alert-type' => 'success'
            );
            
            return redirect()->route('informes')->with($notification);

        } catch (Exception $e) {
            $notification = array(
                'message' => 'Se ha producido un error, intentelo más tarde.',
                'alert-type' => 'error'
            );

            return redirect()->route('create-notification')->with($notification);

        }

    }
}
