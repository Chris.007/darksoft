<?php

namespace App\Http\Controllers;

use App\Models\AssignSubject;
use Illuminate\Http\Request;
use App\Models\Person;
use App\Models\Subject;
use App\Models\Role;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Assign;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        // $person=DB::table('person')
        //             ->select('id_person','id_role','first_name','last_name','email')
        //             ->where('id_person','=',2);
        $subjects = Subject::all();
        $person = Person::find(3);
        $assign = AssignSubject::find([4,5,6,7,8,9]);
        return view('listSubjects', compact('subjects', 'person', 'assign'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subjects = Subject::all();
        $person = Person::find($id);
        $assign = AssignSubject::where('id_person',$id)
            ->join('subject', 'assign_subject.id_subject', 'subject.id_subject')
            ->orderBy('subject.name_subject', 'ASC')
            ->get();
        return view('docenteAuxiliar.myListSubjects', compact('subjects', 'person', 'assign'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
