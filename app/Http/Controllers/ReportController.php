<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ReserveClassroom;
use App\Models\Reserve;

class ReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //todos los reportes aceptados y rechazados
        $reports = Reserve::
        join('reserve_course','form_reserve.id_form', '=','reserve_course.id_form')
        ->join( 'assign_subject', 'reserve_course.id_assign_subject','assign_subject.id_assign_subject' )
        ->join('person', 'assign_subject.id_person', 'person.id_person')
        ->join('subject', 'assign_subject.id_subject', 'subject.id_subject')
        ->join('motive_reserve', 'form_reserve.id_motive', 'motive_reserve.id_motive')
        ->join('reserve_classroom', 'form_reserve.id_form', 'reserve_classroom.id_form')

        //condiciones
        ->where('form_reserve.enviado','=', true)
        ->where('person.id_person','=', $id)
        //->orderBy('reserve_classroom.id_form', 'desc')
        ->get();

        //horarios--rechazado
        $horarioSolicitud = Reserve::
        join('reserve_course','form_reserve.id_form', '=','reserve_course.id_form')
        ->join( 'assign_subject', 'reserve_course.id_assign_subject','assign_subject.id_assign_subject' )
        ->join('person', 'assign_subject.id_person', 'person.id_person')
        ->join('subject', 'assign_subject.id_subject', 'subject.id_subject')
        ->join('motive_reserve', 'form_reserve.id_motive', 'motive_reserve.id_motive')
        ->join('reserve_classroom', 'form_reserve.id_form', 'reserve_classroom.id_form')
        ->join('assign_class_timetable', 'form_reserve.id_form' , 'assign_class_timetable.id_form')
        ->join('class_timetable', 'assign_class_timetable.id_class_timetable', 'class_timetable.id_class_timetable')

        //Seleccion de la columna
        ->select('reserve_classroom.id_reserve', 'assign_class_timetable.id_class_timetable','class_timetable.timetable_range')

        //condiciones
        ->where('form_reserve.enviado','=', true)
        ->where('person.id_person','=', $id)
        ->where('reserve_classroom.state_reserve','=', false)
        ->get();

        //Horarios reservados
        $horarioReserva = Reserve::
        join('reserve_course','form_reserve.id_form', '=','reserve_course.id_form')
        ->join( 'assign_subject', 'reserve_course.id_assign_subject','assign_subject.id_assign_subject' )
        ->join('person', 'assign_subject.id_person', 'person.id_person')
        ->join('subject', 'assign_subject.id_subject', 'subject.id_subject')
        ->join('motive_reserve', 'form_reserve.id_motive', 'motive_reserve.id_motive')

        //con reportes
        ->join('reserve_classroom', 'form_reserve.id_form', 'reserve_classroom.id_form')
        ->join('assign_classroom', 'reserve_classroom.id_reserve','assign_classroom.id_reserve')
        ->join('class_timetable', 'assign_classroom.id_class_timetable', 'class_timetable.id_class_timetable')
        ->join('classroom', 'assign_classroom.id_classroom', 'classroom.id_classroom')

        //Seleccion de la columna
        ->select('assign_classroom.id_class_timetable', 'assign_classroom.id_reserve', 'class_timetable.timetable_range')

        //condiciones
        ->where('form_reserve.enviado','=', true)
        ->where('person.id_person','=', $id)
        ->groupBY('assign_classroom.id_class_timetable', 'assign_classroom.id_reserve', 'class_timetable.timetable_range')
        ->get();

        //Aulas asignadas
        $classRoom = Reserve::
        join('reserve_course','form_reserve.id_form', '=','reserve_course.id_form')
        ->join( 'assign_subject', 'reserve_course.id_assign_subject','assign_subject.id_assign_subject' )
        ->join('person', 'assign_subject.id_person', 'person.id_person')
        ->join('subject', 'assign_subject.id_subject', 'subject.id_subject')
        ->join('motive_reserve', 'form_reserve.id_motive', 'motive_reserve.id_motive')

        //con reportes
        ->join('reserve_classroom', 'form_reserve.id_form', 'reserve_classroom.id_form')
        ->join('assign_classroom', 'reserve_classroom.id_reserve','assign_classroom.id_reserve')
        ->join('class_timetable', 'assign_classroom.id_class_timetable', 'class_timetable.id_class_timetable')
        ->join('classroom', 'assign_classroom.id_classroom', 'classroom.id_classroom')

        //Seleccion de la columna
        ->select('assign_classroom.id_classroom', 'assign_classroom.id_reserve', 'classroom.number_classroom')

        //condiciones
        ->where('form_reserve.enviado','=', true)
        ->where('person.id_person','=', $id)
        ->groupBY('assign_classroom.id_classroom', 'assign_classroom.id_reserve', 'classroom.number_classroom')
        ->get();

        return view('docenteAuxiliar.report')
        ->with('reports', $reports)
        ->with('classRoom', $classRoom)
        ->with('horarioReserva', $horarioReserva)
        ->with('horarioSolicitud', $horarioSolicitud);
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
