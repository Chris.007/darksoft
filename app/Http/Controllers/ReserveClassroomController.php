<?php

namespace App\Http\Controllers;

use App\Models\ReserveClassroom;
use App\Models\AssignClassroom;
use App\Models\AssignClass;
use App\Models\Classroom;
use App\Models\Reserve;
use Illuminate\Http\Request;
use PHPUnit\Exception;

class ReserveClassroomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
    }
    private function getEstadoSolitud($literal){
         return !(strtolower($literal) == 'rechazado');
    }
    public function store(Request $request) {
        try {
            $respuesta = new ReserveClassroom();
            $respuesta->id_form = $request->idSolicitud;
            $respuesta->notified = true;
            $respuesta->state_reserve = $this->getEstadoSolitud($request->estado);
            $respuesta->comments = $request->comentarios;
            $respuesta->save();
            /*$notification = array(
                'info-message' => 'La Notificacion se envio con exito!',
                'alert-type' => 'success'
            );*/

            //dd($request->input('inputId'));

            //GUARDADO DE ASIGNACION DE AULA
            if ($this->getEstadoSolitud($request->estado)) {
            $myClass = $request->idClassroom;
            $classSelect = Classroom::find($myClass);
            $aulas = Classroom::all();
            $myform = Reserve::find($request->idSolicitud);
            $assign = AssignClass::where('id_form', $request->idSolicitud)->get();//Periodo-horario

            
                if ($request->myContinuo == 1) { //SI -> 1
                    $listaIdAulas = Array();        
                    $cadena = $classSelect->continua;
                    $listClassroom = explode (',', $cadena);

                    foreach ($listClassroom as $miNumAula) {
                        $dato = trim($miNumAula);
                        foreach ($aulas as $aulaBD) {
                            if($aulaBD->number_classroom == $dato){
                                array_push($listaIdAulas,$aulaBD->id_classroom);
                            }
                        }
                    }

                    foreach ($listaIdAulas as $classroom) {
                        foreach ($assign as $timeClass) {
                            $assignClass = new AssignClassroom();
                            $assignClass->id_reserve = $respuesta->id_reserve;   
                            $assignClass->id_class_timetable = $timeClass->id_class_timetable;
                            $assignClass->id_classroom = $classroom;
                            $assignClass->date_reserve = $myform->date_reserve;
                            $assignClass->save();    
                        }
                    }
                    
                }else{//NO -> 0
                    foreach ($assign as $timeClass) {
                        $assignClass = new AssignClassroom();
                        $assignClass->id_reserve = $respuesta->id_reserve;   
                        $assignClass->id_class_timetable = $timeClass->id_class_timetable;
                        $assignClass->id_classroom = $myClass;
                        $assignClass->date_reserve = $myform->date_reserve;
                        $assignClass->save();
                    }
                }    
            }
            
                
                $notification = array(
                    'info-message' => 'La Notificacion se envio con exito!',
                    'alert-type' => 'success'
                );

            return redirect()->route('ListInformation.index')->with($notification);

        } catch (Exception $e) {
            $notification = array(
                'info-message' => 'Se ha producido un error, intentelo más tarde.',
                'alert-type' => 'error'
            );

            return redirect()->route('ListInformation.index')->with($notification);

        }

    }

}
