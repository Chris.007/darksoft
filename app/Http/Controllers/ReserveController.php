<?php

namespace App\Http\Controllers;

use App\Models\ClassTime;
use App\Models\Person;
use App\Models\MotiveReserve;
use App\Models\Reserve;
use App\Models\ReserveCourse;
use App\Models\ReservePeriod;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\AssignSubject;
use App\Models\DraftForm;
use App\Models\Subject;
use App\Models\ReserveDraftCourse;
use Illuminate\Support\Facades\Auth;
use League\Flysystem\Exception;
use Illuminate\Support\Facades\Validator;
use function Sodium\add;

class ReserveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {


    }

    public function edit($formId) {
        $idUser= Auth::user()->id;
        $reserva = Reserve::find($formId);
        $cursoReserva = ReserveCourse::where('id_form', $formId)->first();
        $cursoPropietario = AssignSubject::where('id_assign_subject', $cursoReserva->id_assign_subject)->first();
        $cursos = AssignSubject::where('id_subject', $cursoPropietario->subject->id_subject)
            ->where('id_assign_subject', '!=', $cursoPropietario->id_assign_subject)->get();
        $motivos = MotiveReserve::all();
        $periodos = ClassTime::all();

        return view('docenteAuxiliar.editForm',
            ['reserva'=>$reserva, 'def'=> $cursoPropietario, 'cursos' => $cursos, 'motivos'=> $motivos, 'periodos'=>$periodos]);

    }
    public function create($courseId) {

        $course = AssignSubject::find($courseId);
        $courses = AssignSubject::where('id_subject', $course->id_subject)
            ->where('id_assign_subject', '!=', $course->id_assign_subject)->get();
        //dd($courses);
        $motivos = MotiveReserve::all();
        $periodos = ClassTime::all();
        return view('docenteAuxiliar.form',['courses' => $courses, 'def' => $course, 'motivos'=> $motivos, 'periodos'=>$periodos]);
    }

    public function store(Request $request) {
        if ($request->input('action') == 'editar') {
            $notBorrador = $this->draftStore($request);
            return redirect()->route('myListDrafts.show', 3)->with($notBorrador);

         }
        else {
            $validator = Validator::make($request->all(), [
                'solicitantes'  => 'required',
                //'grupos' => 'required',
                'estudiantes'  => ['required', 'numeric', 'min:10', 'max:500'],
                'motivo'  => 'required',
                'fecha'  => 'required | date | after : tomorrow',
                'periodos'  => 'required | array | max:5',
            ], [
                'solicitantes.required' => 'campo requerido',
                'estudiantes.required' => 'campo requerido',
                //'grupos.required' => 'campo requerido',
                'fecha.required' => 'campo requerido',
                'motivo.required' => 'campo requerido',
                'periodos.required' => 'campo requerido',
                'estudiantes.min' => 'La cantidad minima de estudiantes pemitida es 10.',
                'estudiantes.max' => 'La cantidad maxima de estudiantes pemitida es 500.',
                'fecha.after' => 'Selecione una fecha hábil, ejemplo fecha hoy 20/12/ fecha hábil 22/12/',
                'periodos.max' => 'Seleccione menos de 5 periodos',
            ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $idUser = Auth::user()->id;
            $existe = $request->motivo == 1 ?
                $this->existeExamenReserva($request->solicitantes, $request->fecha)
                : $this->existeClasePresencialReserva($request->solicitantes, $request->fecha, $request->periodos);
            if (is_array($existe) && count($existe) > 0) {
                $grupos = implode(', ',$existe);
                $notification = array(
                    'info-message' => sprintf('No se envio la reserva. Ya existe una solicitud con el mismo periodo o fecha para los grupos: %s', $grupos),
                    'alert-type' => 'error'
                );
                return redirect()->route('list-subjects.show', $idUser)->with($notification);
            }

            try {
                $reserva = new Reserve();
                if($request->id_reserva != null) {
                    $reserva = Reserve::find($request->id_reserva);
                }
                $reserva->id_person = $idUser;
                $reserva->id_motive = $request->motivo;
                $reserva->date_reserve = $request->fecha ?? Carbon::now()->addDays(2);
                $reserva->number_students = $request->estudiantes;
                $reserva->continuous_classroom = $request->continuous_classroom;
                $reserva->enviado = true;

                $reserva->save();
                ReservePeriod::where('id_form', $reserva->id_form)->delete();
                ReserveCourse::where('id_form', $reserva->id_form)->delete();
                foreach ($request->periodos as $periodo) {
                    $newReservePeriod = new ReservePeriod();
                    $newReservePeriod->id_form = $reserva->id_form;
                    $newReservePeriod->id_class_timetable = $periodo;
                    $newReservePeriod->save();
                }
                foreach ($request->solicitantes as $solicitante) {
                    $newReservePeriod = new ReserveCourse();
                    $newReservePeriod->id_form = $reserva->id_form;
                    $newReservePeriod->id_assign_subject = $solicitante;
                    $newReservePeriod->save();
                }


                $notification = array(
                    'info-message' => 'La Reserva se envio con exito!',
                    'alert-type' => 'success'
                );

                return redirect()->route('list-subjects.show',$idUser)->with($notification);

            } catch (Exception $e) {
                error_log($e->getMessage());
                $notification = array(
                    'info-message' => 'Se ha producido un error, intentelo más tarde.',
                    'alert-type' => 'error'
                );

                return redirect()->route('create.reserve', $request->solicitantes[0])->with($notification);
            }
        }
    }

    public function draftStore (Request $request) {

        try {
            $reserva = new Reserve();
            if($request->id_reserva != null) {
                $reserva = Reserve::find($request->id_reserva);
            }
            $reserva->id_person = Auth::user()->id;
            $reserva->id_motive = $request->motivo;
            $reserva->date_reserve = $request->fecha ?? Carbon::now()->addDays(2);
            $reserva->number_students = $request->estudiantes;
            $reserva->continuous_classroom = $request->continuous_classroom;
            $reserva->save();
            ReservePeriod::where('id_form', $reserva->id_form)->delete();
            ReserveCourse::where('id_form', $reserva->id_form)->delete();
            if ($request->periodos != null) {
                foreach ($request->periodos as $periodo) {
                    $newReservePeriod = new ReservePeriod();
                    $newReservePeriod->id_form = $reserva->id_form;
                    $newReservePeriod->id_class_timetable = $periodo;
                    $newReservePeriod->save();
                }
            }
            if ($request->solicitantes != null){
                foreach ($request->solicitantes as $solicitante) {
                    $newReservePeriod = new ReserveCourse();
                    $newReservePeriod->id_form = $reserva->id_form;
                    $newReservePeriod->id_assign_subject = $solicitante;
                    $newReservePeriod->save();
                }
            }
            return array(
                'info-message' => 'La Reserva se Guardo con exito!',
                'alert-type' => 'success'
            );
        } catch (Exception $e) {
            error_log($e->getMessage());
            return array(
                'info-message' => 'Se ha producido un error, intentelo más tarde.',
                'alert-type' => 'error'
            );
        }
    }

    public function getDraft(Request $request) {

        $draft = DraftForm::find($request);

        $course = AssignSubject::find(2);
        $motive = MotiveReserve::find(2);
        $period = ReservePeriod::find($course->id_class_timetable);

        $courses = AssignSubject::where('id_subject', $course->id_subject)
            ->where('id_person', '!=', $course->person->id_person)->get();

        $motives = MotiveReserve::all();
        $periods = ClassTime::all();


        return view('docenteAuxiliar.form',['courses' => $courses, 'def' => $course, 'motivos' => $motives, 'periodos' => $periods, 'draft' => $draft]);
    }

    private function existeExamenReserva($solicitantes, $fecha)
    {
        if (count($solicitantes) == 0) return false;
        $res = array();
        $reservas = Reserve::whereDate('date_reserve', $fecha)
            ->where('id_motive', 1)
            ->where('enviado', true)
            ->whereHas('cursos', function($q) use ($solicitantes) {
            $q->whereIn('assign_subject.id_assign_subject', $solicitantes);
        })->get();
        if (count($reservas) == 0) return false;
        foreach ($reservas as $reserva) {
            $cursosReserva = $reserva->cursos->pluck('id_assign_subject')->toArray();
            $res = array_merge($res, $cursosReserva);
        }
        $cursosIds = array_unique($res);
        $cursosDuplicados= array_intersect($cursosIds, $solicitantes);
        $numeroGrupos = AssignSubject::whereIn('id_assign_subject', $cursosDuplicados)->pluck('group_number')->toArray();
        return $numeroGrupos;
    }
    private function existeClasePresencialReserva($solicitantes, $fecha, $periodos)
    {
        if (count($solicitantes) == 0) return false;
        $gruposRepetidos = array();
        $periodosRepetidos = array();
        $reservas = Reserve::whereDate('date_reserve', $fecha)
            ->where('enviado', true)
            ->whereHas('cursos', function($q) use ($solicitantes) {
                $q->whereIn('assign_subject.id_assign_subject', $solicitantes);
            })
            ->whereHas('periodos', function($q) use ($periodos) {
                $q->whereIn('class_timetable.id_class_timetable', $periodos);
            })->get();
        if (count($reservas) == 0) return false;
        foreach ($reservas as $reserva) {
            $cursosReserva = $reserva->cursos->pluck('id_assign_subject')->toArray();
            $gruposRepetidos = array_merge($gruposRepetidos, $cursosReserva);

            $periodosReserva = $reserva->periodos->pluck('id_class_timetable')->toArray();
            $periodosRepetidos = array_merge($periodosRepetidos, $periodosReserva);
        }
        $cursosIds = array_unique($gruposRepetidos);
        $cursosDuplicados= array_intersect($cursosIds, $solicitantes);
        $numeroGrupos = AssignSubject::whereIn('id_assign_subject', $cursosDuplicados)->pluck('group_number')->toArray();

        return $numeroGrupos;
    }

}
