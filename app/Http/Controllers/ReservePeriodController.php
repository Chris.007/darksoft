<?php

namespace App\Http\Controllers;

use App\Models\ReservePeriod;
use Illuminate\Http\Request;

class ReservePeriodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReservePeriod  $reservePeriod
     * @return \Illuminate\Http\Response
     */
    public function show(ReservePeriod $reservePeriod)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReservePeriod  $reservePeriod
     * @return \Illuminate\Http\Response
     */
    public function edit(ReservePeriod $reservePeriod)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReservePeriod  $reservePeriod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReservePeriod $reservePeriod)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReservePeriod  $reservePeriod
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReservePeriod $reservePeriod)
    {
        //
    }
}
