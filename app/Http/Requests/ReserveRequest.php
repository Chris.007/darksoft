<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReserveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'solicitantes'  => 'required',
            'grupos' => 'required',
            'estudiantes'  => ['required', 'numeric', 'min:10', 'max:500'],
            'motivo'  => 'required',
            'fecha'  => 'required | date | after : tomorrow',
            'periodos'  => 'required | array | max:5',
        ];
    }
    public function messages(): array
    {
        return [
            'solicitantes.required' => 'campo requerido',
            'estudiantes.required' => 'campo requerido',
            'grupos.required' => 'campo requerido',
            'fecha.required' => 'campo requerido',
            'motivo.required' => 'campo requerido',
            'periodos.required' => 'campo requerido',
            'estudiantes.min' => 'La cantidad minima de estudiantes pemitida es 10.',
            'estudiantes.max' => 'La cantidad maxima de estudiantes pemitida es 500.',
            'fecha.after' => 'Selecione una fecha hábil, ejemplo fecha hoy 20/12/ fecha hábil 22/12/',
            'periodos.max' => 'Seleccione menos de 5 periodos',
        ];
    }
}
