<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssignClass extends Model
{
    //use HasFactory;
    protected $table = 'assign_class_timetable';
    protected $primaryKey = 'id_assign_class_timetable';
    protected $foreignSubject = 'id_form';
}
