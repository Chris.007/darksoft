<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssignClassroom extends Model
{
    //use HasFactory;
    protected $table = 'assign_classroom';
    protected $primaryKey = 'id_assign_classroom';
    protected $foreignReserveClassroom = 'id_reserve';
    protected $foreignClassTime = 'id_class_timetable';
    protected $foreignClassroom = 'id_classroom';
    protected $fillable = [
        'id_assign_classroom','id_reserve', 'id_class_timetable','id_classroom','date_reserve', 'updated_at'
    ];


}
