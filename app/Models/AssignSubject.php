<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AssignSubject
 *
 * @property-read \App\Models\Person|null $persons
 * @method static \Illuminate\Database\Eloquent\Builder|AssignSubject newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AssignSubject newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AssignSubject query()
 * @mixin \Eloquent
 */
class AssignSubject extends Model
{
    // use HasFactory;
    protected $table = 'assign_subject';
    protected $primaryKey = 'id_assign_subject';
    protected $foreignPerson = 'id_person';
    protected $foreignSubject = 'id_subject';
    protected $fillable = [
        'id_person','id_subject','group_number', 'registered_number'
    ];
/*
    public function persons()
    {
        // return $this->hasOne('App\Models\Person', 'id_person', 'id_person');
        return $this->hasOne(Person::class);
    }
*/
    public function person()
    {
        return $this->belongsTo(Person::class, 'id_person', 'id_person');
    }
    public function subject()
    {
        return $this->belongsTo(Subject::class, 'id_subject', 'id_subject');
    }
    public function reservas()
    {
        return $this->belongsToMany(Reserve::class, 'reserve_course', 'id_assign_subject', 'id_form')->withTimestamps();
    }
}
