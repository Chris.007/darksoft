<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassPeriodo extends Model
{
    //use HasFactory;
    protected $table = 'class_timetable';
    protected $primaryKey = 'id_class_timetable';
    protected $fillable = [
        'timetable_range'
    ];

}
