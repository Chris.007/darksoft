<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class ClassTime extends Model
{
    protected $table = 'class_timetable';
    protected $primaryKey = 'id_class_timetable';
    protected $fillable = [
        'id_class_timetable','timetable_range'
    ];

    public function reservas()
    {
        return $this->belongsToMany(Reserve::class, 'assign_class_timetable', 'id_class_timetable', 'id_form')->withTimestamps();
    }

    public function reservasClassroom()
    {
        return $this->belongsToMany(ReserveClassroom::class, 'assign_classroom', 'id_class_timetable', 'id_reserve')->withTimestamps();
    }

}
