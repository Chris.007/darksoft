<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    //use HasFactory;
    protected $table = 'classroom';
    protected $primaryKey = 'id_classroom';
    protected $fillable = [
        'id_classroom', 'number_classroom','capacity', 'type_classroom','continua', 'capacidadac', 'idcontinuo'
    ];

    public function reservasClassroom()
    {
        return $this->belongsToMany(ReserveClassroom::class, 'assign_classroom', 'id_classroom', 'id_reserve')->withTimestamps();
    }
}
