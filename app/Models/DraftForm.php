<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\draftForm
 *
 * @method static \Illuminate\Database\Eloquent\Builder|draftForm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|draftForm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|draftForm query()
 * @mixin \Eloquent
 */
class DraftForm extends Model
{

    protected $table = 'draft_form';
    protected $primaryKey = 'id_draft_form';
    protected $foreignPerson = 'id_person';
    protected $foreignMotive = 'id_motive';

    protected $fillable = [
        'date_reserve_df', 'number_students_df', 'continuous_classroom'
    ];
}
