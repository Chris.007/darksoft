<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Person
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Person newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Person newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Person query()
 * @mixin \Eloquent
 */
class Person extends Model
{
    protected $table = 'person';
    protected $primaryKey = 'id_person';
    protected $foreign = 'id_role';
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password_email'
    ];
    
    public function courses()
    {
        return $this->hasMany(AssignSubject::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'id_role', 'id_role');
    }
}
