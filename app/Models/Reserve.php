<?php

namespace App\Models;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Reserve
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Reserve newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Reserve newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Reserve query()
 * @mixin \Eloquent
 */
class Reserve extends Model
{
    protected $table = 'form_reserve';
    protected $primaryKey = 'id_form';
    protected $foreignPerson = 'id_person';
    protected $foreignMotive = 'id_motive';
    protected $fillable = ['id_form','id_motive','date_reserve','number_students','continuous_classroom','enviado'];

    public function cursos()
    {
        return $this->belongsToMany(AssignSubject::class, 'reserve_course', 'id_form', 'id_assign_subject')->withTimestamps();
    }
    public function periodos()
    {
        return $this->belongsToMany(ClassTime::class, 'assign_class_timetable', 'id_form', 'id_class_timetable')->withTimestamps();
    }
    public function person()
    {
        return $this->belongsTo(Person::class, 'id_person', 'id_person');
    }
    public function motive()
    {
        return $this->belongsTo(Motive::class, 'id_motive', 'id_motive');
    }
    public function getTimestamp($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }
    public function getDate($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }
}
