<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReserveClassroom extends Model
{
    protected $table = 'reserve_classroom';
    protected $primaryKey = 'id_reserve';
    protected $foreignReserve = 'id_form';
    protected $fillable = [
        'id_reserve', 'id_form', 'notified', 'state_reserve', 'comments', 
    ];

    public function form()
    {
    return $this->belongsTo(Reserve::class, 'id_form', 'id_form');
    }
    public function classroom()
    {
        return $this->belongsToMany(Classroom::class, 'assign_classroom', 'id_reserve', 'id_classroom')->withTimestamps();
    }

    public function periodos()
    {
        return $this->belongsToMany(ClassTime::class, 'assign_classroom', 'id_reserve', 'id_class_timetable')->withTimestamps();
    }

}