<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ReserveCourse extends Model
{
    protected $table = 'reserve_course';
    protected $primaryKey = 'id_reserve_course';
    protected $foreignReserve = 'id_form';
    protected $foreignAssignSubject = 'id_assign_subject';
    protected $fillable = [
        'id_form','id_assign_subject'
    ];
}
