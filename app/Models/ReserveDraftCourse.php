<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReserveDraftCourse extends Model
{
    protected $table = 'reserve_draf_course';
    protected $primaryKey = 'id_draf_course';
    protected $foreignReserveDraft = 'id_reserve_draf';
    protected $foreignCourse = 'id_course';

}
