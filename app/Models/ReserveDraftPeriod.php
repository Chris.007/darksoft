<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReserveDraftPeriod extends Model
{
    protected $table = 'assign_draft_class_timetable';
    protected $primaryKey = 'id_assign_draft_class_timetable';
}
