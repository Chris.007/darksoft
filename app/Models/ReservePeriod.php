<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ReservePeriod
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ReservePeriod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReservePeriod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReservePeriod query()
 * @mixin \Eloquent
 */
class ReservePeriod extends Model
{
    protected $table = 'assign_class_timetable';
    protected $primaryKey = 'id_assign_class_timetable';
    protected $fillable = [ 'id_form', 'id_class_timetable'];
}
