<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Subject
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Subject newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subject newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subject query()
 * @mixin \Eloquent
 */
class Subject extends Model
{
    // use HasFactory;
    protected $table = "subject";
    protected $primaryKey = "id_subject";
    protected $fillable = [
        'name_subject'
    ];
    public function courses()
    {
        return $this->hasMany(AssignSubject::class);
    }
}
