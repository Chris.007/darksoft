    const cancelarReserva = () => {
    if ($('#inputFecha').val() !== '' || $('#inputPeriodos').val().length > 0) {
    $('#modalCancelar').modal('show');
} else {
    window.location.href = "{{ route('list-subjects.show',$idUser)}}";
}
}
    $('#inputSolicitante').select2({
    tags: true,
    templateSelection : function (tag, container){
    // here we are finding option element of tag and
    // if it has property 'locked' we will add class 'locked-tag'
    // to be able to style element in select
    var $option = $('.select2 option[value="'+tag.id+'"]');
    if ($option.attr('locked')){
    $(container).addClass('locked-tag');
    tag.locked = true;
    }
    return tag.text;
    },
    })
    .on('select2:unselecting', function(e){
    // before removing tag we check option element of tag and
    // if it has property 'locked' we will create error to prevent all select2 functionality
    if ($(e.params.args.data.element).attr('locked')) {
    e.select2.pleaseStop();
    }
    });
    $('#inputGrupos').select2({
    tags: true,
    disabled:'readonly'
    });
    $("#inputSolicitante").on('change', function (e) {
    const coursesIds = $('#inputSolicitante').select2('data');
    const jsCourses = courses;
    const courseIds = coursesIds.map(c => parseInt(c.id));
    const selectedGroups = jsCourses.filter(c => courseIds.includes(c.id_assign_subject));
    $("#inputGrupos").empty();
    let estudiantes = 0;
    for (const group of selectedGroups) {
    const $newOption = $("<option selected></option>").val(group.group_number).text(group.group_number)
    $("#inputGrupos").append($newOption);
    estudiantes += group.registered_number;
}
    $("#inputGrupos").trigger('change');
    $('#inputEstudiantes').val(estudiantes);
    $("#inputEstudiantes").attr({
    "max" : estudiantes,
    "min" : 10
});
});



