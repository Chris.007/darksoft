$(document).ready(function () {
    jQuery.validator.addMethod("noDomingo", function(value) {
        const test =new Date(value).getDay();
        return test !== 6;
    }, "No puede reservar para este dia.");
    jQuery.validator.addMethod("after", function(value) {
        let fechaActual2 = new Date()
        fechaActual2.setDate(fechaActual2.getDate() + 2);
        return (new Date(value) >= fechaActual2);
    }, "Envie la reserva con dos dias de anticipacion.");

    jQuery.validator.addMethod("continuo", function(value) {
        const ordered = value.sort(function(a, b) {
            return a - b;
        });
        return ordered[ordered.length -1 ] - ordered[0] === ordered.length -1;
    }, "Selecciona peridos continuos.");

    const validobj = $('#testF').validate({
        ignore: [],
        errorClass: 'is-invalid',
        rules: {
            estudiantes: {
                required: true,
                number: true,
                min: 10,
                max: 500
            },
            fecha: {
                required: true,
                date: true,
                after: true,
                noDomingo:true
            },
            "periodos[]": {
                required: true,
                maxlength: 3,
                continuo: true
            },
        },
        messages: {
            estudiantes: {
                required: "Campo requerido.",
                number: "Ingrese solo numeros.",
                min: "La cantidad minima de estudiantes permitida es 10.",
                max: "La cantidad maxima de estudiantes permitida es 500."
            },
            fecha: {
                required: "Campo requerido.",

            },
            "periodos[]": {
                required: "Campo requerido.",
                maxlength: "Solo puede ingresar 3 periodos."
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },

        errorPlacement: function(label, element) {
            if (element.hasClass('mul-select')) {
                label.insertAfter(element.next('.select2-container')).addClass('mt-2 text-danger');
                select2label = label
            } else {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            }
        },
        success: function(label, element) {
            $(element).removeClass('is-invalid')
            label.remove();
        },
    });
    $("#inputPeriodos").on("change", function(e) {
        validobj.form();
    });
});
