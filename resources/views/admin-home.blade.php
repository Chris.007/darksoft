@extends('layouts.app')

@section('content')
<div class="row">
        <div>
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                     {{ session('status') }}
                </div>
            @endif
                <div class = "container">
                     @if (Auth::user()->id_role === 1)
                       <H1 class="text-center">
                        Bienvenido Administrador {{Auth::user()->name}}
                        </H1>
                    @elseif(Auth::user()->id_role === 2)
                     <H1 class="text-center">
                            Bienvenido Docente {{Auth::user()->name}}
                        </H1>
                    @else
                        <H1 class="text-center">
                            Bienvenido Auxiliar {{Auth::user()->name}}
                        </H1>
                   @endif
                </div>
             @include('layouts.includes.body')
</div>
@endsection
