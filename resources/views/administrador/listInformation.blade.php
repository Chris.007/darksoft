@extends('layouts.app')

@section('content')

<style>
  body {
    min-height: 100vh;
    display: grid;
    grid-template-rows: auto auto 1fr auto ;
    font-family: 'Poppins', sans-serif;
  }

  table {
    font-family: 'Poppins', sans-serif;
    color:black;
    font-weight: normal !important;
  }

  strong {
    font-weight: 900;
  }

</style>

<body>
  <div id="pricing" class="container ">
    <br>
    <h5 class="fw-normal" style="color: #215F88"> Mi lista de informes emitidos </h5>
    <br>
    @if (!$listInfo->isEmpty())
    <br>
    <table class="table datatable table-hover table-responsive table-bordered table-striped border-white">
      <thead>
        <tr class=" fw-lighter" style="color: #215F88">
          <th class=" text-center" scope="col">Nro</th>
          <th class=" text-center" scope="col">Personal</th>
          <th class=" text-center" scope="col">Solicitante</th>
          <th class=" text-center" scope="col">Materia</th>
          <th class=" text-center" scope="col">Nro Estudiantes</th>
          <th class=" text-center" scope="col">Fecha</th>
          <th class=" text-center" scope="col">Estado</th>
          <th class=" text-center" scope="col">Acción</th>
        </tr>
      </thead>
      <tbody>
        @php
          $num = 1;
        @endphp
        @foreach ($listInfo as $info)
          <tr class=" align-middle">
            <td class="text-center align-middle">{{$num++}}</td>
            <td class="text-center align-middle">{{{$info->form->person->role->name_role}}}</td>
            <td>{{{$info->form->person->first_name}}} {{{$info->form->person->last_name}}}</td>
            <td scope="row">{{$info->form->cursos->first()->subject->name_subject ?? ''}}</td>
            <td class="text-center align-middle">{{$info->form->number_students}}</td>
            <td class="text-center align-middle">{{$info->form->date_reserve}}</td>
              @if($info->state_reserve == 1)
                <td class="text-center align-middle"><span class="badge rounded-pill bg-success">Aceptado</span></td>
                <td class="table-light text-center align-middle">
                  <button
                    type="button"
                    class="btn"
                    style="background-color: #215F88; color: white; padding: 0 8px 0 8px"
                    data-bs-toggle="modal"
                    data-bs-target="#exampleModal"
                    data-full-name="{{{$info->form->person->role->name_role}}} {{{$info->form->person->first_name}}} {{{$info->form->person->last_name}}}"
                    data-subject="{{$info->form->cursos->first()->subject->name_subject ?? ''}}"
                    data-number-students="{{$info->form->number_students}} estudiantes"
                    data-reserve-date="{{$info->form->date_reserve}}"
                    data-reserve-state="ACEPTADO"
                    >
                      Ver más
                  </button>
                </td>
              @else
                <td class="text-center align-middle"><span class="badge rounded-pill bg-danger">Rechazado</span></td>
                <td class="table-light text-center align-middle">
                  <button
                    type="button"
                    class="btn"
                    style="background-color: #215F88; color: white; padding: 0 8px 0 8px"
                    data-bs-toggle="modal"
                    data-bs-target="#exampleModal"
                    data-full-name="{{{$info->form->person->role->name_role}}} {{{$info->form->person->first_name}}} {{{$info->form->person->last_name}}}"
                    data-subject="{{$info->form->cursos->first()->subject->name_subject ?? ''}}"
                    data-number-students="{{$info->form->number_students}} estudiantes"
                    data-reserve-date="{{$info->form->date_reserve}}"
                    data-reserve-state="RECHAZADO"
                    >
                    Ver más
                  </button>
                </td>
              @endif
          </tr>
        @endforeach
      </tbody>
      <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content text-center">
              <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Informe de Reserva</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                La solicitud de reserva del
                <strong class='modal-full-name' id="recipient-fullname"></strong> de la materia
                <strong id="recipient-subject"></strong> con
                <strong id="recipient-number-students"></strong> en fecha
                <strong id="recipient-date"></strong> ha sido
                <strong id="recipient-state"></strong>.
              </div>
              <div class="modal-footer align-middle">
                <button type="button" class="btn" style="background-color: #215F88; color: white" data-bs-dismiss="modal">Volver a Informes</button>
              </div>
            </div>
          </div>
        </div>

        <script>
          var exampleModal = document.getElementById('exampleModal')
          exampleModal.addEventListener('show.bs.modal', function (event) {
            var button = event.relatedTarget

            var recipientFullName = button.getAttribute('data-full-name')
            var recipientSubject = button.getAttribute('data-subject')
            var recipientNumberStudents = button.getAttribute('data-number-students')
            var recipientReserveDate = button.getAttribute('data-reserve-date')
            var recipientReserveState = button.getAttribute('data-reserve-state')

            var modalBodyInput = exampleModal.querySelector('.modal-body input')
            var modalBodyStrong = exampleModal.querySelector('#recipient-fullname')
            var modalBodySubject = exampleModal.querySelector('#recipient-subject')
            var modalBodyNumberStudents = exampleModal.querySelector('#recipient-number-students')
            var modalBodyReserveDate = exampleModal.querySelector('#recipient-date')
            var modalBodyReserveState = exampleModal.querySelector('#recipient-state')

            modalBodyStrong.textContent = recipientFullName
            modalBodySubject.textContent = recipientSubject
            modalBodyNumberStudents.textContent = recipientNumberStudents
            modalBodyReserveDate.textContent = recipientReserveDate
            modalBodyReserveState.textContent = recipientReserveState
          })
        </script>
    </table>

  </div>
    @else
      <br>
          <div class="text-center">
              <img src="https://firebasestorage.googleapis.com/v0/b/adaroom-3619c.appspot.com/o/solicitudes.jpg?alt=media&token=d1a8f3f3-bdc0-4dea-ad66-2f412d566870" class="rounded" width="100px" height="120px" >
          </div>
          <p class="text-center text-muted" style="color: #000000; font-size: 14px" >No se encontró ningún informe</p>
    @endif
<br>
<br>
</body>
@endsection
