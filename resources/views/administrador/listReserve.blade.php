@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ asset('css/classroom.css') }}">
<body>
    <div class="container ">
        <br>
        <h5 class="fw-normal" style="color: #215F88"> Mi lista de solicitudes de reserva de aula </h5>
        @if (!$reservas->isEmpty())
        <br>
        <table class="table datatable table-hover table-responsive table-bordered border-white table-striped">
            <thead>
                <tr class=" fw-lighter" style="color: #215F88">
                <th scope="col" class=" text-center">Nro</th>
                <th scope="col">Solicitante</th>
                <th scope="col">Materia</th>
                <th scope="col">Motivo</th>
                <th scope="col">Grupo(s)</th>
                <th class=" text-center" scope="col">Fecha Reserva</th>
                <th class=" text-center" scope="col">Fecha Solicitud</th>
                <th class=" text-center" scope="col">Estado</th>
                <th class=" text-center" scope="col">Acción</th>
                </tr>
            </thead>
            <tbody>
            @php
                $num = 1;
            @endphp
            @foreach ($reservas as $reserva)
            @php
                $miColor = "badge rounded-pill bg-danger";
                $estado ="";
                $fecha_entrada = date("d-m-Y", strtotime($reserva->date_reserve));
                $fecha_ahora = date("d-m-Y",time());
                $fecAsemana = date("d-m-Y",strtotime($fecha_ahora."+ 1 week"));
                $fecha_semana = strtotime($fecAsemana);
                $fecha_en = strtotime($fecha_entrada);
                $fecha_ah = strtotime($fecha_ahora);

                if($fecha_ah > $fecha_en){
                    $estado ="Vencido";
                    $miColor = "badge bg-danger rounded-pill text-white";
                }else{
                    if ($fecha_ah < $fecha_en &&  $fecha_en < $fecha_semana) {
                        $estado ="Urgente";
                        $miColor = "badge bg-warning text-dark";
                    }else {
                        $estado ="Vigente";
                        $miColor = "badge bg-success ";
                    }
                }
            @endphp
                    <tr>
                        
                        <td scope="row" class=" text-center" >{{$num}}</td>
                        <td>{{{$reserva->person->first_name}}} {{{$reserva->person->last_name}}}</td>
                        <td scope="row">{{$reserva->cursos->first()->subject->name_subject ?? ''}}</td>
                        <td scope="row">{{$reserva->motive->type_motive }}</td>
                        @php
                            $grupos = "";
                            foreach($reserva->cursos as $curso) {
                                $grupos = $grupos.", ".$curso->group_number;
                            }
                        $dataGrupos = trim($grupos,',');
                        @endphp

                        <td>{{$dataGrupos}}</td>
                        <td>{{$fecha_entrada}}</td>
                        <td>{{$reserva->getTimestamp($reserva->updated_at)}}</td>
                        <td>
                            <span class="{{$miColor}}">
                                {{$estado}}
                            </span>
                        </td>
                        <td class=" text-center">
                            <form action="{{route('show.ListReserve', $reserva->id_form)}}" method="get">
                                <button type="submit" class="btn" style="background: #215F88; color:white; padding: 0 8px 0 8px;">Ver Solicitud</button>
                            </form>
                        </td>
                    </tr>
                    @php
                        $num = $num +1;
                    @endphp
                @endforeach

            </tbody>
        </table>
    </div>

    @else
        <br>
        <div class="text-center">
            <img src="https://firebasestorage.googleapis.com/v0/b/adaroom-3619c.appspot.com/o/a_solicitud.png?alt=media&token=fe39a0b2-64e6-4a50-8e88-7c3aea07f60c" class="rounded" width="100px" height="120px" >
        </div>
        <p class="text-center text-muted" style="color: #000000; font-size: 14px" >No se encontró ninguna solicitud</p>
    @endif
    <script>
        const d = new Date();
        const ye = new Intl.DateTimeFormat('es', { year: 'numeric' }).format(d);
        const mo = new Intl.DateTimeFormat('es', { month: 'short' }).format(d);
        const da = new Intl.DateTimeFormat('es', { day: 'numeric' }).format(d);
        const diaSemana = new Intl.DateTimeFormat('es', { weekday: 'long' }).format(d);
        const hr = new Intl.DateTimeFormat('es', { hour: 'numeric' }).format(d);
        const sg = new Intl.DateTimeFormat('es', { second: 'numeric' }).format(d);
        //const tz = new Intl.DateTimeFormat('es', { timeZone: ''}).format(d);
        const test = `${diaSemana}, ${da} ${mo}, ${ye} ${hr}:${sg}`;
        console.log(test);
        $("#inputFechaAsignacion").text(test);
    </script>
    <br>
    <br>
</body>
@endsection
