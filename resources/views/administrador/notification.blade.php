@extends('layouts.app')
@section('content')
<div>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" id="not" data-toggle="modal" data-target="#exampleModalCenter">
        enviar notificacion
    </button>
    {{ csrf_field() }}
    <script type="text/javascript">
        $('#not').on('click', function() {
            $('#exampleModalCenter').modal('show');
        });
    </script>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter"  data-bs-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Enviar Notificacion</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form class="container">
                        <div class="mb-3">
                                <label for="inputEstado" class="fw-light">Estado Solicitud </label>
                                <div class="border border-gray rounded p-1 d-inline-flex" name="estado" id="inputEstado" >
                                    <span class="badge bg-secondary">Aceptado</span>
                                </div>
                        </div>
                    <!--
                        <div class="row mb-3">
                                <label for="inputFechaReserva" class="fw-light">Fecha Solicitud</label>
                                <br/>
                                <div class="border border-gray rounded p-1 d-inline-flex" id="inputFechaAsignacion" >
                                    <span >{{$solicitud->date_reserve}}</span>
                                </div>

                        </div>
                        <div class="row mb-3">
                            <label for="inputAulas" class="fw-light">Aulas Asignadas</label><br>
                            <div class="border border-gray rounded p-1" name="aulas" id="inputAulas" >
                                <span class="badge bg-secondary">690 A</span>
                                <span class="badge bg-secondary">690 B</span>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label for="inputPeriodos" class="fw-light">Periodos</label>
                            <div class="border border-gray rounded p-1" id="inputPeriodos" >
                                <span class="badge bg-secondary">6.45 - 8.15</span>
                            </div>

                        </div>
                        -->
                        <div class="row mb-3">
                            <label for="inputNota" class="fw-bold">Nota</label>
                            <textarea class="form-control" id="inputNota" rows="3" placeholder="Comentarios..."></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary">Enviar</button>
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        const d = new Date();
        const ye = new Intl.DateTimeFormat('es', { year: 'numeric' }).format(d);
        const mo = new Intl.DateTimeFormat('es', { month: 'short' }).format(d);
        const da = new Intl.DateTimeFormat('es', { day: 'numeric' }).format(d);
        const diaSemana = new Intl.DateTimeFormat('es', { weekday: 'long' }).format(d);
        const hr = new Intl.DateTimeFormat('es', { hour: 'numeric' }).format(d);
        const sg = new Intl.DateTimeFormat('es', { second: 'numeric' }).format(d);
        //const tz = new Intl.DateTimeFormat('es', { timeZone: ''}).format(d);
        const test = `${diaSemana}, ${da} ${mo}, ${ye} ${hr}:${sg}`;
        console.log(test);
        $("#inputFechaAsignacion").text(test);
    </script>
@endsection
