@extends('layouts.app')

@section('content')

<body>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/classroom.css') }}">
    @php
        $listaHorarios = Array(); //periodos [1,2,3]
        $periodoR = "";
        foreach($assignClassroom as $assign) {
            foreach($periodos as $periodo) {
                if (($periodo->id_class_timetable) == ($assign->id_class_timetable)) {
                    $periodoR = $periodoR.' ; '.$periodo->timetable_range;
                    array_push($listaHorarios,$periodo->id_class_timetable);
                }
            }
        }
        $periodoReserve = trim($periodoR,' ; ');

    @endphp

    <div class="container">
        <div class="row mx-md-n5">
            <div class="col info-booking shadow mb-1">                
                <br>
                <h5 class="text-center fw-bold" style="color: #215F88" >DETALLE DE LA RESERVA DE AULA</h5>
                <br>
                <p><b>Solicitante:</b> <span class="border rounded-pill p-1">{{$docente->first_name}} {{$docente->last_name}}</span></p>
                <p><b>Materia: </b><span class="border rounded-pill p-1">{{$materia->name_subject}}</span></p>
                <p><b>Fecha:</b><span class="border rounded-pill p-1">{{$detalle->getDate($detalle->date_reserve)}}</span></p>
                <p><b>Periodos:</b> <span class="border rounded-pill p-1">{{$periodoReserve}}</span></p>
                <p><b>Motivo: </b> <span class="border rounded-pill p-1">{{$motivo->type_motive}}</span></p>
                <p><b>Cantidad de estudiantes:</b> <span class="border rounded-pill p-1">{{$detalle->number_students}}</span></p>
                @php
                    $motivo = "Si";
                    if (($detalle->continuous_classroom) == 0) {
                        $motivo = "No";
                    }
                @endphp
                <p><b>¿Desea aulas continuas?: </b><span class="border rounded-pill p-1">{{$motivo}}</span></p>
            
            </div>
            <div class="col-8 px-md-5">
            
                <br>
                <h5 class="fw-bold" style="color: #215F88" >AULAS DISPONIBLES</h5>
                <br>
                @if (!$listClassroom->isEmpty() || !$continuo->isEmpty())
                <table class="table datatable table-hover table-responsive table-bordered border-white table-striped" >
                    <thead>
                        <tr class="fw-lighter" style="color: #215F88">
                            <th scope="col" class=" text-center">Nro.</th>
                            <th scope="col">Aula</th>
                            <th scope="col">Capacidad</th>
                            <th scope="col">Seleccionar</th>
                        </tr>
                    </thead>
        
                    @php
                        //periodos [1,2,3] solicitadas
                        $excluirAula = Array();
                        if ($existeFecha){
                            foreach ($listaHorarios as $horario) {
                                foreach ($listaFechas as $fecha) {
                                    if ($fecha->id_class_timetable == $horario) {
                                        if (!in_array($fecha->id_classroom, $excluirAula)) {
                                            array_push($excluirAula,$fecha->id_classroom);//1-> 622
                                        }
                                    }
                                }
                            }
                        }
                    $num = 1;
                    $respuestaSolicitud= false;
                    @endphp
                    <tbody>
                    <!--Script para validar check radio y desabilitar Notificar-->
                    @foreach ($listClassroom as $classroom)
                    @if (!in_array($classroom->id_classroom, $excluirAula))
                        <tr>
                            <td scope="row" class=" text-center">{{$num++}}</td>
                        <td>{{$classroom->number_classroom}}</td>  <!-- NECESITAMOS EL AULA FILTRADO -->
                        <td>{{$classroom->capacity}}</td>
                        <td>
                            <input class="form-check-input" type="radio" onclick="setRespuesta({{$classroom->id_classroom}},0)" name="flexRadioDefault" id="flexRadioDefault1"/></td>
                        </tr>
                    @endif
                    @endforeach
                    @if(($detalle->continuous_classroom) && ($listClassroom->isEmpty()))
                        @foreach ($continuo as $cont)
                            @if (!in_array($cont->id_classroom, $excluirAula))
                                <tr>
                                <td>{{$num++}}</td>
                                <td>{{$cont->continua}}</td>  <!-- NECESITAMOS EL AULA FILTRADO -->
                                <td>{{$cont->capacidadac}}</td>
                                <td>
                                    <input class="form-check-input" type="radio" onclick="setRespuesta({{$cont->id_classroom}},1)"  name="flexRadioDefault" id="flexRadioDefault1"/></td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                    </tbody>
                </table>
                @else
                <br>
                    <div class="text-center">
                        <img src="https://firebasestorage.googleapis.com/v0/b/adaroom-3619c.appspot.com/o/aula.png?alt=media&token=9a61ecf9-245a-4222-b2a5-a0c046825593" class="rounded" width="100px" height="100px" >
                    </div>
                    <p class="text-center text-muted" style="color: #000000; font-size: 14px" >No se encontró aulas disponibles...</p>
                    <br>
                    <br>
                    <br>
                @endif
                    <div class=" text-center mx-auto justify-content: center mt-5">
                        <button type="button"style="margin-right:20%;" class="btn btn-danger me-md-5 btn-space"  data-bs-toggle="modal" data-bs-target="#staticBackdrop" style="background-color: #D22929;">Cancelar</button>
                        <button type="submit" id="Notificar" style="margin-left:20%; background-color: #215F88; color: white" class="btn" name="action" value="enviar" onclick="mostrarNotificacion()">Notificar</button>
                    </div>
            </div>
        </div>

        <!-- Modal Cancelar-->
        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                ¿Está usted seguro(a) de cancelar la asignación de aula?. Se borrarán los cambios.
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                <button type="button" style="background-color: #215F88;" class="btn btn-primary"data-bs-dismiss="modal" onclick="window.history.back()">Si</button>
            </div>
            </div>
            </div>
        </div>
    </div>



    <!-- Modal notificacion -->
    <div class="modal fade" id="notificacionModal"  data-bs-backdrop="static" >
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Enviar Notificacion</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form class="container" method="post" action="{{ route('respuestas.store') }}">
                        {{ csrf_field() }}
                        <div>
                            <div class="mb-3">
                                <label for="inputId" class="fw-light">Solicitud</label>
                                <input type="text" class="form-control" id="inputId" readonly value={{$detalle->id_form}} name="idSolicitud">
                            </div>


                            <input type="text" class="form-control" id="idClassroom" name="idClassroom" value="0" hidden> <!--ID AULA SELECCIONADA -->
                            <input type="text" class="form-control" id="myContinuo" name="myContinuo" value="0" hidden> <!--1 Si es continuo (0 NO)-->

                            <div class="mb-3">
                                <label for="inputEstado" class="fw-light">Estado</label>
                                <div style="width: 7.5rem">
                                    <input type="text" class="form-control bg-danger text-white" id="inputEstado" readonly value="Rechazado" name="estado">
                                </div>
                            </div>

                            <div class="mb-3">
                                <label for="inputNota" class="fw-bold">Nota</label>
                                <textarea class="form-control" id="inputNota" rows="3" placeholder="Comentarios..." name="comentarios">No se han encontrado Aulas Disponibles con los requerimientos de su solicitud.</textarea>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn" style="background-color: #215F88; color: white">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        const setRespuesta = (classroom,esContinuo) => {
            $("#inputEstado").val('Aceptado');
            $("#inputEstado").removeClass('bg-danger');
            $("#inputEstado").addClass('bg-success');
            $("#idClassroom").val(classroom);// OBTENER ID AULA
            $("#myContinuo").val(esContinuo);//es continuo?
            $('#inputNota').val('');
        }
        const mostrarNotificacion = () => {
            $('#notificacionModal').modal('show');
        }
    </script>
<br>
<br>
</body>
@endsection
