
@extends('layouts.app')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css" integrity="sha256-3sPp8BkKUE7QyPSl6VfBByBroQbKxKG7tsusY2mhbVY=" crossorigin="anonymous" />
@section('content')
<link href="{{ asset('css/login.css') }}" rel="stylesheet">
<div class="container">
    <div class="row justify-content-center wrapper">
        <h5 class="fw-normal text-center" style="color: #215F88"> Ingresar al Sistema</h5>
        <h6 class="fw-normal text-center" style="color: #624B09"> Todos los campos son requeridos(*)</h6>
            <form method="POST" class ="mt-3" action="{{ route('login') }}">
                    @csrf
                        <div class="col-12">
                                <div class="input-group align-items-center ">
                                    <div class="col-2" >
                                        <h4><i class="bi bi-person-fill" style="color: black"></i></h4>
                                    </div>

                                    <div class="col-10" >
                                        <label name = "email" for="email">{{ __('Correo: ')}}<span class="text-danger">*</span></label>
                                    </div>
                                </div>

                                <div class="col-12 align-items-center text-center">
                                    <input id="email"type="email"class="form-field @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"placeholder="micorreo@dominio.com" >
                                    @error('email')
                                        <span class="invalid-feedback " role="alert">
                                                <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    @if ($errors->has('periodos'))
                                    <span class="text-danger">{{ $errors->first('periodos') }}</span>
                                @endif

                                </div>
                        </div>

                        <div class="col-12">
                            <div class="col-12 input-group align-items-center">
                                    <div class="col-2">
                                        <h4><i class="bi bi-lock-fill" style="color: black"></i></h4>
                                    </div>
                                    <div class="col-10">
                                        <label for="password" name="password">{{ __('Contraseña: ')}}<span class="text-danger">*</span></label>
                                    </div>
                            </div>
                                    <div class="col-12  align-items-center text-center">
                                        <input id="password" type="password" class="form-field @error('password') is-invalid @enderror" name="password"placeholder="mi contraseña">

                                        @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                        </div>

                             <div class="col-12 text-center ">

                                 <button type="submit" id="submit"  class="btn btn-lg btn-custom btn-block mx-aut @error('ingresar') is-invalid @enderror"style="background: #215F88;  color:white">
                                     {{ __('Ingresar') }}
                                 </button>

                                 @error('ingresar')
                                     <span class="invalid-feedback" role="alert">
                                         <strong>{{ $message }}</strong>
                                     </span>
                                 @enderror

                                 @if (Route::has('password.request'))

                                 @endif
                          </div>

                    </form>

         </div>
</div>
@endsection





