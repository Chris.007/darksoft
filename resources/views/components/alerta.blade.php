<div class="modal fade" id="modalCancelar">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <div class="modal-body">
                ¿Seguro que desea cancelar la reserva?, se perderan los cambios.
            </div>

            <div class="modal-footer">
                <button type="button" class="btn" data-bs-dismiss="modal" style="background: #215F88; color:white">
                    No
                </button>
                <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('list-subjects.show',$user) }}'">
                    Si
                </button>
            </div>
        </div>
    </div>
</div>
