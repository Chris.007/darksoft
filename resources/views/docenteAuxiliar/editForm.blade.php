@extends('layouts.app')

@section('content')


    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <style>
        .locked-tag .select2-selection__choice__remove{
            display: none!important;
        }
    </style>
    <div class="container p-0" >
        <br/>
        @if (session('alert-type') == 'success')
            @push('scripts')
                <script type="text/javascript">
                    jQuery(function($) {
                        $('#sendModal').modal('show');
                    });
                </script>
            @endpush
        @endif

        @php
            $idUser= Auth::user()->id;
        @endphp

        <div class="d-flex justify-content-center">
            <div class="col-8">
                <h3 class="text-center mt-1" style="color: #215F88;">Editar Reserva - {{$reserva->id_form}}</h3>
                <p class="text-secondary mt-2 p-2"> Todos los campos son requeridos para enviar una solicitud de reserva</p>
                <form id="testF" class="needs-validation border rounded p-3" novalidate method="post" action="{{ route('reserves.store') }}">
                    {{ csrf_field() }}
                    <input type="text" name="id_reserva" value={{$reserva->id_form}} hidden>
                    <div class="mb-3">
                        <label for="inputSolicitante" class="form-label fw-bold">Nombre Solicitante(s)</label>
                        <select class="multi-select form-control" multiple name="solicitantes[]" id="inputSolicitante" style="width: 100%">
                            <option selected locked="locked" value={{$def->id_assign_subject}}>{{$def->person->first_name}} {{$def->person->last_name}}</option>
                            @foreach($cursos as $curso)
                                @if(in_array($curso->id_assign_subject, $reserva->cursos->pluck('id_assign_subject')->toArray()))
                                    <option selected value={{$curso->id_assign_subject}}>{{$curso->person->first_name}} {{$def->person->last_name}}</option>
                                    @else
                                    <option value={{$curso->id_assign_subject}}>{{$curso->person->first_name}} {{$def->person->last_name}}</option>
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('solicitantes'))
                            <span class="text-danger">{{ $errors->first('solicitantes') }}</span>
                        @endif
                    </div>
                    <!--<div class="mb-3">
                        <label for="inputMateria" class="form-label">Materia</label>
                        <input type="text" class="form-control" id="inputMateria" disabled readonly value={{$def->subject->name_subject}} name="materia">
                    </div>-->
                    <div class="mb-3">
                        <label for="inputMateria" class="form-label">Materia</label>
                        <div class="card">
                            <div class="card-body" style="padding: 5px;">{{$def->subject->name_subject}}</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg mb-3">
                            <label for="inputGrupos" class="form-label">Grupo(s)</label>
                            <select class="multi-select form-control" multiple name="grupos[]" id="inputGrupos" >
                                <option selected locked="locked" value={{$def->group_number}}>{{$def->group_number}}</option>
                                @foreach($cursos as $curso)
                                    @if(in_array($curso->id_assign_subject, $reserva->cursos->pluck('id_assign_subject')->toArray()))
                                    <option selected value={{$curso->group_number}}>{{$curso->group_number}}</option>
                                    @else
                                        <option value={{$curso->group_number}}>{{$curso->group_number}}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if ($errors->has('grupos'))
                                <span class="text-danger">{{ $errors->first('grupos') }}</span>
                            @endif
                        </div>
                        <div class="col-lg mb-3">
                                <label for="inputEstudiantes" class="form-label">Estudiantes</label>
                                <input
                                    type="number"
                                    class="form-control"
                                    id="inputEstudiantes"
                                    name='estudiantes'
                                    value={{$reserva->number_students ?? ''}}>
                                @if ($errors->has('estudiantes'))
                                    <span class="text-danger">{{ $errors->first('estudiantes') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg mb-3">
                                <label for="inputMotivo" class="form-label">Motivo</label>
                                <select class="form-select" id="inputMotivo" name="motivo">
                                    @foreach($motivos as $motivo)
                                        @if($motivo->id_motive == $reserva->id_motive)
                                            <option selected value={{$motivo->id_motive}}>{{$motivo->type_motive}}</option>
                                        @else
                                            <option value={{$motivo->id_motive}}>{{$motivo->type_motive}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('motivo'))
                                    <span class="text-danger">{{ $errors->first('motivo') }}</span>
                                @endif

                            </div>
                            <div class="col-lg mb-3">
                                <label for="inputFecha" class="form-label">Fecha</label>
                                <input type="date" class="form-control" id="inputFecha" name="fecha" value={{$reserva->date_reserve}}>
                                @if ($errors->has('fecha'))
                                    <span class="text-danger">{{ $errors->first('fecha') }}</span>
                                @endif

                            </div>
                        </div>
                        <div class="mb-3 col-md-12">
                            <label for="inputPeriodos" class="form-label">Periodo(s)</label>
                            <select class="mul-select form-control" multiple name="periodos[]" id="inputPeriodos" style="width: 100%">
                                @foreach($periodos as $periodo)
                                    @if(in_array($periodo->id_class_timetable, $reserva->periodos->pluck('id_class_timetable')->toArray()))
                                        <option selected value={{$periodo->id_class_timetable}}>{{$periodo->timetable_range}}</option>
                                    @else
                                        <option value={{$periodo->id_class_timetable}}>{{$periodo->timetable_range}}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if ($errors->has('periodos'))
                                <span class="text-danger">{{ $errors->first('periodos') }}</span>
                            @endif

                        </div>
                        <div class="row mb-3">
                            <div class="col" style="text-align: end; padding: 0 22px 0 0">
                                <label class="col form-label">Si no se encuentra un aula, <br> ¿Podemos sugerirle aulas continuas ?</label>
                            </div>

                            <div class="col" style="text-align:start ; padding: 23px 0 0 0" >
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="continuous_classroom" id="checkYes" value="1">
                                    <label class="form-check-label" for="inlineRadio1">Si</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="continuous_classroom" id="checkNo" value="0">
                                    <label class="form-check-label" for="inlineRadio2">No</label>
                                </div>
                                    @if ($errors->has('continuous_classroom'))
                                        <span class="text-danger">{{ $errors->first('continuous_classroom') }}</span>
                                    @endif
                            </div>
                        </div>
                    <!--Botones-->
                    <div class="d-flex justify-content-end gap-2">
                        <button type="submit" class="btn btn-warning" name="action" value="editar">Guardar en Borrador</button>
                        <button type="button" class="btn btn-secondary" onclick="cancelarReserva()">Cancelar</button>
                        <button type="submit" style="background-color: #215F88; color: white" class="btn" name="action" value="enviar">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <x-alerta :user="$idUser"/>
    <script>
        const courses = {{ Js::from($cursos->push($def))}};
        const reservaAulaContinua = {{Js::from($reserva->continuous_classroom)}};
    </script>
    <script src="{{ asset('js/components/form.js')}}" ></script>
    <script src="{{ asset('js/components/validation.js')}}" ></script>
    <script>
        const aulaContinua = reservaAulaContinua;
        if (aulaContinua) {
            $('#checkYes').prop('checked', true);
        } else {
            $('#checkNo').prop('checked', true);
        }

    </script>
@endsection
