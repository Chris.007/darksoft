@extends('layouts.app')

@section('content')
<style>
    body{
      display: grid;
      font-family: 'Poppins', sans-serif;
    }
    table{
      font-family: 'Poppins', sans-serif;
      color:black;
      font-weight: normal !important;
    }
    .h2{
      font: bold;
    }
</style>
<body>
  <div class="container">
    <br>
    <h5 class="row fw-normal" style="color: #215F88"> Mi lista de borradores </h5>
    @if (!$reservas->isEmpty())
    <br>
      <div>
          <table class="table datatable table-hover table-responsive table-bordered border-white">
              <thead>
              <tr class=" fw-lighter" style="color: #215F88">

                  <th scope="col" class=" text-center">Nro</th>
                  <th scope="col">Materia</th>
                  <th scope="col">Grupo(s)</th>
                  <th scope="col">Cant. de inscritos</th>
                  <th scope="col">Ultima Modificacion</th>
                  <th scope="col">Editar</th>
                  <!-- <th scope="col">Eliminar</th> -->
              </tr>
              </thead>
              <tbody>
              @php
                $num=1;
              @endphp
              @foreach ($reservas as $reserva)
                  <tr>
                      <!--<td scope="row" class=" text-center">{{$reserva->id_form}}</td>-->
                      <td scope="row" class=" text-center">{{$num++}}</td>

                      <td scope="row">{{$reserva->cursos->first()->subject->name_subject ?? ''}}</td>
                      @php
                          $grupos = "";
                          foreach($reserva->cursos as $curso) {
                              $grupos = $grupos.",".$curso->group_number;
                          }
                      $dataGrupos = trim($grupos,',');
                      @endphp

                      <td >{{$dataGrupos}}</td>
                      <td>{{$reserva->number_students}}</td>
                      <td>{{$reserva->updated_at}}</td>
                      <td style="align-items: center">
                          <a href="{{route('reserves.edit', $reserva->id_form)}}">
                              <h4><i class="bi bi-pencil-square" style="color: black"></i></h4>
                          </a>
                      </td>
                  <!-- <td style="align-items: center"><a href="{{route('home')}}"><img src="/images/eliminar.png" width="28px" height="28px"></a></td> -->
                  </tr>
              @endforeach
              </tbody>
          </table>
      </div>
    @else
      <br>
      <div class="text-center">
        <img src="https://firebasestorage.googleapis.com/v0/b/adaroom-3619c.appspot.com/o/save_3621.png?alt=media&token=fb4588ce-5547-4866-82de-207e1c29f831" class="rounded" width="100px" height="100px" >
      </div>
      <p class="text-center text-muted" style="color: #000000; font-size: 14px" >No se encontró ninguna borrador</p>
    @endif
  </div>
</body>
@endsection
