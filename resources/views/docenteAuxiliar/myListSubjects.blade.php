@extends('layouts.app')

@section('content')

    <style>
        body{
            min-height: 100vh;
            display: grid;
            grid-template-rows: auto auto 1fr auto ;
            font-family: 'Poppins', sans-serif;
        }
        table{
            font-family: 'Poppins', sans-serif;
            color:black;
            font-weight: normal !important;
            overflow-x: auto;
            white-space: nowrap;
        }

    </style>
<body>
    <div id="pricing" class="container">
        <br>
        <h5 class="fw-normal" style="color: #215F88"> Mi lista de materias </h5>
        <br>
        <table class="table datatable table-hover table-responsive table-bordered border-white">
            <thead>
                <tr class=" fw-lighter" style="color: #215F88">
                <th class=" text-center" scope="col">Materia</th>
                <th class=" text-center" scope="col">Grupo</th>
                <th class=" text-center" scope="col">Cant. de Inscritos</th>
                <th class=" text-center" scope="col">Acción</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($assign as $ass )
                <tr>
                @foreach ($subjects as $subject)
                    @if(($subject->id_subject) == ($ass->id_subject))
                    <td><img src="/images/icono.png" width="24px" height="24px">&nbsp; &nbsp;{{{$subject->name_subject}}}</td>
                    @endif
                @endforeach

                @foreach($subjects as $subject)
                    @if(($subject->id_subject) == ($ass->id_subject))
                    <td class=" text-center">{{$ass->group_number}}</td>
                    @endif
                @endforeach

                @foreach($subjects as $subject)
                    @if(($subject->id_subject) == ($ass->id_subject))
                    <td class=" text-center">{{$ass->registered_number}}</td>
                    @endif
                @endforeach

                @foreach($subjects as $subject)
                    @if(($subject->id_subject) == ($ass->id_subject))
                    <td class=" text-center">
                        <form action="{{route('create.reserve', $ass->id_assign_subject)}}" method="get">
                        <button type="submit" class="btn" style="background: #215F88; color:white; padding: 0 8px 0 8px ">Reservar Aula
                    </button></form>

                    </td>
                    @endif
                @endforeach

                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</body>
@endsection
