@extends('layouts.app')

@section('content')

<style>
    body{
        min-height: 100vh;
        display: grid;
        grid-template-rows: auto auto 1fr auto ;
        font-family: 'Poppins', sans-serif;
    }
    table{
        font-family: 'Poppins', sans-serif;
        color:black;
        font-weight: normal !important;
    }
</style>
<body>
    <div id="pricing" class="container table-responsive">
        <br>
        <h5 class="fw-normal" style="color: #215F88"> Mi lista de reportes </h5>
        @if (!$reports->isEmpty())
        <br>
            <table class="table datatable align-middle ">
                <thead>
                    <tr class="fw-lighter" style="color: #215F88">
                        <th class=" text-center" scope="col">Nro</th>
                        <th class=" text-center" scope="col">Materia</th>
                        <th scope="col">Periodos</th>
                        <th scope="col">Fecha Reserva</th>
                        <th scope="col">Aulas Asignadas</th>
                        <th scope="col">Comentarios</th>
                        <th class=" text-center" scope="col">Estado</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $num= 1;
                    @endphp
                    @foreach ($reports as $rep)
                        <tr>
                            <!--<td>{{$rep->id_reserve}}</td>-->
                            <td class=" text-center">{{$num++}}</td>
                            <td>{{$rep->name_subject}}</td>

                            @php
                                $hora = "";
                                foreach ($horarioReserva as $horario) {
                                    if ($horario->id_reserve == $rep->id_reserve) {
                                        $hora = $hora.'; '.$horario->timetable_range;
                                    }
                                }
                                $horaReserve = trim($hora,'; ');
                            @endphp
                            @php
                                $hora = "";
                                foreach ($horarioSolicitud as $horario) {
                                    if ($horario->id_reserve == $rep->id_reserve) {
                                        $hora = $hora.'; '.$horario->timetable_range;
                                    }
                                }
                                $horaSolicitud = trim($hora,'; ');
                            @endphp
                            @if($rep->state_reserve == 1)
                                <td>{{$horaReserve}}</td>
                            @else
                                <td>{{$horaSolicitud}}</td>
                            @endif
                            <td>{{$rep->getDate($rep->date_reserve)}}</td>
                            @php
                                $aula = "";
                                foreach ($classRoom as $class) {
                                    if ($class->id_reserve == $rep->id_reserve) {
                                        $aula = $aula.'; '.$class->number_classroom;
                                    }
                                }
                                $aulaReserve = trim($aula,'; ');
                            @endphp
                            @if($rep->state_reserve == 1)
                                <td>{{$aulaReserve}}</td>
                            @else
                                <td class="text-danger">Ninguna</td>
                            @endif
                            <td>{{$rep->comments}}</td>

                            @if($rep->state_reserve == 1)
                                <td class="text-center align-middle"><span class="badge rounded-pill bg-success">Aceptado</span></td>
                            @else
                                <td class="text-center align-middle"><span class="badge rounded-pill bg-danger">Rechazado</span></td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <br>
            <div class="text-center">
                <img src="https://firebasestorage.googleapis.com/v0/b/adaroom-3619c.appspot.com/o/reporte.png?alt=media&token=e5c63002-d7c5-4d0a-bac1-eb7839e7d3ec" class="rounded" width="120px" height="120px" >
            </div>
            <p class="text-center text-muted" style="color: #000000; font-size: 14px" >No se encontró ningun reporte</p>
        @endif
    </div>
</body>
@endsection
