<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('SAA') }}</title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    @yield('css')
    <!-- Styles Y BOOSTRAP -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{{ asset('css/navbar.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">

    <!-- TIPO DE LETRA -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">

    <!-- Select Periodo -->
    @stack('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

    <link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" rel="stylesheet">

    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>


    @stack('scripts')
    <style>

        table tr th {
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .sorting {
            background-color: #fffb01;
        }
        .asc:after {
            content: ' ▲';
        }

        .desc:after {
            content: " ▼";
        }
        .resaltar {
            cursor: default;
            color: crimson;
        }
        .active { background-color: #fff;}
        .page-item.active .page-link {
            color: #fff !important;
            background-color: #215F88 !important;
            border-color: #215F88 !important;
        }

    </style>
</head>

@include('layouts.includes.header')


<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg margin"style=" background-color: #C4C4C4;">
            <div class="container">
                    <a class="navbar-brand" href="{{ url('/home') }}"></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}" style="box-shadow: 0 0 0 0">
                        <span class="" ><img src="/images/detalle.png" width="27px" height="27px"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent" style="align-items: center">
                        <div class="col">
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0" style="align-content: center;">
                                <li class="nav-item" style="margin-right: 5%;">
                                    <a class="menu nav-link" href="{{route('myHome')}}"><h5 style="color: #000000;"><b>INICIO</b></h5></a>
                                </li>

                                @if(Auth::check())
                                    @if (Auth::user()->id_role === 1)
                                            <!--ADMINISTRADOR-->

                                        <li class="nav-item" style="margin-right: 5%;">
                                            <a class="menu nav-link" href="{{route('ListReserve.index')}}" ><h5 style="color: #000000;"><b>SOLICITUDES</b></h5></a>
                                        </li>
                                        <li class="nav-item" style="margin-right: 5%;">
                                            <a class="menu nav-link" href="{{route('ListInformation.index')}}" ><h5 style="color: #000000;"><b>INFORMES</b></h5></a>
                                        </li>
                                    @endif
                                    @if (Auth::user()->id_role === 2 ||Auth::user()->id_role === 3)
                                    <!--DOCENTE/AUXILIAR-->
                                        @php
                                            $id= Auth::user()->id;
                                        @endphp

                                        <li class="nav-item" style="margin-right: 5%;">
                                            <a class="menu nav-link" href="{{ route('list-subjects.show',$id) }}"><h5 style="color: #000000;"><b>MATERIAS</b></h5></a>
                                        </li>
                                        <li class="nav-item" style="margin-right: 5%;">
                                            <a class="menu nav-link" href="{{ route('myListDrafts.show',$id) }}"><h5 style="color: #000000;"><b>BORRADORES</b></h5></a>
                                        </li>
                                        <li class="nav-item" style="margin-right: 5%;">
                                            <a class="menu nav-link" href="{{ route('reports.show',$id) }}"><h5 style="color: #000000;"><b>REPORTES</b></h5></a>
                                        </li>
                                    @endif
                                @else

                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse collapse navbar-collapse" id="navbarSupportedContent">
                        <div class="bd-highlight">
                            <ul class="navbar-nav">
                                <!-- Authentication Links -->

                                @guest

                                    @if (Route::has('login'))
                                        <li class=" nav-item">
                                            <a class=" menu nav-link"  href="{{ route('login') }}"><h5 style=" color: #000000;"><b>INGRESAR</b></h5></a>
                                        </li>
                                    @endif

                                    <!--@if (Route::has('register'))
                                        <li class="nav-item active"style="margin-right: 5%;">
                                            <a class="nav-link menu" href="{{ route('register') }}" style="margin-right: 5%;"><h5 style=" color: #000000;">REGISTRAR</h5></a>
                                        </li>
                                    @endif     -->
                                    @else
                                    <li class="nav-item dropdown">

                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="color: #000000">
                                            {{ Auth::user()->name }} {{ Auth::user()->last_name }}
                                        </a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown" style="background-color: #d4d4d4; padding-left: 8px;">
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                                {{ __('Cerrar Sesion') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                @csrf
                                            </form>
                                        </ul>
                                    </li>
                                @endguest
                            </ul>
                        </div>
                    </div>

            </div>
        </nav>
        @if (session('info-message'))
            <div class="overflow-visible position-fixed bottom-0 end-0 p-3" style="z-index: 11">
                <div class="toast hide bg-white" role="alert" data-bs-delay=10000>
                    <div class="toast-header d-flex flex-row-reverse">
                        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                    <div class="d-flex align-items-center m-3">
                        @switch(session('alert-type'))
                            @case('success')
                            <h4><i class="bi bi-check-circle-fill" style="color: green"></i></h4>
                            @break
                            @case('info')
                            <h4><i class="bi bi-info-circle-fill" style="color: blue"></i></h4>
                            @break
                            @case('warning')
                            <h4><i class="bi bi-exclamation-circle-fill" style="color: yellow"></i></h4>
                            @break
                            @case('error')

                            <h4><i class="bi bi-x-circle-fill" style="color: red"></i></h4>
                            @break
                            @default
                        @endswitch
                        <div class="toast-body ms-2">
                            {{session('info-message')}}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>


        <main class="py-4">
            @yield('content')
        </main>

    @include('layouts.includes.footer')



    <!-- Select Periodo -->
    <script>
        $(document).ready(function() {
            $('.multi-select').select2({
                tags:true
            });
        });
        $(document).ready(function(){
            $(".mul-select").select2({
                    tags: true,
                    tokenSeparators: ['/',',',';'," "]
                });
            $('.toast').toast('show')

        $(".datatable").DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            "lengthMenu": [[5,10,50,-1], [5,10,50,"Todos"]],
            "paging": true
        });
        })
    </script>

    <script>
        $(document).ready(() => {
            $('th').each(function(columna) {
                $(this).hover(function() {
                    $(this).addClass('resaltar');
                }, function() {
                    $(this).removeClass('resaltar');
                });
            });
        });
        $('th').click(function() {
            var table = $(this).parents('table').eq(0)
            var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
            this.asc = !this.asc
            if (!this.asc) {
            rows = rows.reverse()
            }
            for (var i = 0; i < rows.length; i++) {
            table.append(rows[i])
            }
            setIcon($(this), this.asc);
        })

        function comparer(index) {
            return function(a, b) {
            var valA = getCellValue(a, index),
                valB = getCellValue(b, index)
            return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
            }
        }

        function getCellValue(row, index) {
            return $(row).children('td').eq(index).html()
        }

        function setIcon(element, asc) {
            $("th").each(function(index) {
            $(this).removeClass("sorting");
            $(this).removeClass("asc");
            $(this).removeClass("desc");
            });
            element.addClass("sorting");
            if (asc) element.addClass("asc");
            else element.addClass("desc");
        }
        $(function(){
            // this will get the full URL at the address bar
            const url = window.location.href; 

            // passes on every "a" tag 
            $("a").each(function() {
                    // checks if its the same on the address bar
                if(url == (this.href)) { 
                    $(this).closest("li").addClass("active");
                }
            });
        });
    </script>

    @yield('js')
</body>
</html>
