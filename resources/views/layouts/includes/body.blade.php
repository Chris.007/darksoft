<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Body</title>

    <link rel="stylesheet" href="{{ asset('css/stylesWeb.css') }}">
    <link rel="stylesheet" media="only screen and (min-width: 320px)(max-width: 480px)" href="{{ asset('css/stylesMobil.css') }}">
    <link rel="stylesheet" media="only screen (min-width: 768px) and (max-width: 1024px)" href="{{ asset('css/stylesTablet.css') }}">
<body>
    <div class="container pt-3 pb-3 mb-3 mt-4">
        <div class="row">
            <div class="col-lg-6 col-sm-12 text-center justify-content-center">
                <div id="carouselExampleCaptions" class="carousel slide " data-bs-ride="carousel">
                    <div class="carousel-indicators">
                      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                          <img src="https://firebasestorage.googleapis.com/v0/b/tis2022.appspot.com/o/decanato.jpeg?alt=media&token=81cb6379-6594-47bf-85bc-69c40d0a9191" class = "styleDecanato img-fluid">
                        </div>
                        <div class="carousel-item">
                        <img src="https://firebasestorage.googleapis.com/v0/b/tis2022.appspot.com/o/edificio%20nuevo.jpeg?alt=media&token=4b222fb7-3e9c-4968-ad6e-e04a57dac46d" class = "styleDecanato img-fluid">
                        </div>
                        <div class="carousel-item">
                         <img src="https://firebasestorage.googleapis.com/v0/b/tis2022.appspot.com/o/area%20verde%20UMSS.png?alt=media&token=e5af3aa1-266b-460f-b70e-6f05c339f11c" class = "styleDecanato img-fluid">
                        </div>
                      </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Next</span>
                    </button>
                  </div>

            </div>

            <div class="col-lg-6 col-sm-12 ">
                <br><br><br>
                <p class="h7 margin-text-center"> La Facultad de Ciencias y Tecnología de la Universidad Mayor de San Simón alberga al interior de su Campus, una serie de reconocidos Centros de Investigación, Laboratorios y Ambientes Académicos de estudio y recreación.<br>
                <br> Esta aplicación web permite apoyar en el proceso de asignación de aulas para la toma de exámenes.
                </p>
            </div>

        </div>
    </div>
    <div>
        @yield('contentbody')
    </div>
</body>
</html>
