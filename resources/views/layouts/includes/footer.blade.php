<!DOCTYPE html>
<head>
    <title>Footer</title>
    <link rel="stylesheet" href="{{ asset('css/footer.css') }}">
</head>

<footer class="text-center text-lg-start p-3" style="background-color: #000128;">
  <div class="row" >
        <!--Grid column-->
            <div class="col-lg-7 p1 fon">
                <p>
                    UMSS - Universidad Mayor de San Simón <br>
                    FCYT - Facultad Ciencias y Tecnología<br>
                    DARKSOFT TECHNOLOGICAL SOLUTIONS S.R.L.<br>
                    Copyright © 2022 . Todos los derechos reservados.
                </p>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-lg-5 p1 fon">
                <p> Otros sitios de interés:
                    <a class="sitio" href="http://umss.edu.bo/"> UMSS</a>
                    <a class="sitio" href="http://websis.umss.edu.bo/"> | WebSISS | </a>
                    <a class="sitio" href="http://www.fcyt.umss.edu.bo/"> FCyT | </a>
                    <a class="sitio" href="http://enlinea.umss.edu.bo/"> Plataformas Virtuales | </a>
                    <a class="sitio" href="https://www.facebook.com/UmssBolOficial/">Facebook </a>
                    <br>
                    Email Darksof S.R.L.: darksoftsrl@gmail.com
                </p>
            </div>
            <!--Grid column-->
        </div>
</footer>

</html>
