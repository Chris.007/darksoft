<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/stylesWeb.css') }}">
    <link rel="stylesheet" media="only screen and (min-width: 320px)(max-width: 780px)" href="{{ asset('css/stylesMobil.css') }}">
    <link rel="stylesheet" media="only screen (min-width: 768px) and (max-width: 1024px)" href="{{ asset('css/stylesTablet.css') }}">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Ms+Madi&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Hind:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">

</head>
<body>
    <div class="m-0">
        <div class="row margin" style="background-color: #000128;">
            <div class="row m-0">
                <div class="col-2">
                    <div class="col-sm-12 text-center">
                        <img src="https://firebasestorage.googleapis.com/v0/b/tis2022.appspot.com/o/LogoUMSS.png?alt=media&token=d5579d5e-2f07-43b8-9283-a74d51d3acf3" class="styleLogoUmss">
                    </div>
                </div>
                <div class="col-8 row align-items-center left-content center main-title">
                    <div class="col-sm-12 text-light text-left">
                        <p class="h4">Sistema de Asignación de Aulas</p>
                        <p class="h5">Facultad  Ciencias  y  Tecnología</p>
                        <p class="h5">Universidad Mayor de San Simón</p>
                    </div>
                </div>
                <div class="col-2">
                    <div class="col-sm-12 text-center">
                        <img src="https://firebasestorage.googleapis.com/v0/b/tis2022.appspot.com/o/Logo_Tecno.png?alt=media&token=26845d21-5098-459c-9763-77d470a32a62" class="styleLogoFcyt">
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
