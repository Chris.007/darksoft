<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ListReserva;
use App\Http\Controllers\PersonController;
use App\Http\Controllers\ReserveController;
use App\Http\Controllers\ListDraftsController;
use App\Http\Controllers\ListInformationController;
use \App\Http\Controllers\ReserveClassroomController;
use \App\Http\Controllers\ReportController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//home
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home'); //RUTA USUARIO
Route::get('admin/home', [App\Http\Controllers\HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');// RUTA ADMI
Route::get('/home', function () {
    return view('home');
})->name('myHome');//VISTA HOME INICIAL(sin iniciar sesion)

//list subjects
Route::resource('list-subjects', PersonController::class);
Route::get('/list-subjects', [PersonController::class,'index'])->name('subjects');

// reserve form
Route::resource('reserves', ReserveController::class);

// reserve form draf
Route::get('draft', [ReserveController::class,'draftStore'])->name('draft');
Route::get('reserves/create/{courseId}', [ReserveController::class, 'create', ])->name('create.reserve');

// lista de borradores
Route::resource('myListDrafts', ListDraftsController::class);

// Lista de reportes
Route::resource('reports', ReportController::class);

/*Administrador*/
//lista de reservas
Route::resource('ListReserve', ListReserva::class);
Route::resource('ListReserve', ListReserva::class)->middleware('is_admin');
Route::resource('respuestas', ReserveClassroomController::class)->middleware('is_admin');
Route::get('ListReserve/show/{formId}', [ListReserva::class, 'show', ])->name('show.ListReserve')->middleware('is_admin');

Route::resource('ListInformation', ListInformationController::class)->middleware('is_admin');

Route::post('ListReserve/filter', [ListReserva::class,'filter'])->name('filter')->middleware('is_admin');

Route::get('create-notification', [NotificationController::class,'create'])->middleware('is_admin');
