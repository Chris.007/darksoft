--Tabla role
create table role(
                     id_role   serial primary key,
                     name_role varchar(20) not null unique
);

--Tabla materia
create table subject(
                        id_subject   serial primary key,
                        name_subject varchar(50) not null unique
);

--Tabla Aula
create table classroom(
                          id_classroom       serial primary key,
                          number_classroom   varchar(10) not null unique,
                          capacity           integer not null,
                          type_classroom     varchar(20) not null
);

--Tabla horario de clase Periodo
create table class_timetable(
                                id_class_timetable serial primary key,
                                timetable_range    varchar(15) not null unique
);

--Tabla motivo de la reserva
create table motive_reserve(
                               id_motive   serial primary key,
                               type_motive varchar(50) not null unique
);

--Tabla usuario
create table person(
                       id_person     serial primary key,
                       id_role       integer not null,
                       first_name    varchar(50) not null,
                       last_name     varchar(50) not null,
                       email         varchar(50) not null,
                       password_email  varchar(20) not null unique,
                       FOREIGN KEY (id_role) REFERENCES role(id_role)
);

--Tabla asignacion de materia
create table assign_subject(
                               id_assign_subject serial primary key,
                               id_person         integer not null,
                               id_subject        integer not null,
                               group_number      varchar(10) not null,
                               registered_number integer not null,
                               FOREIGN KEY (id_person) REFERENCES person(id_person),
                               FOREIGN KEY (id_subject) REFERENCES subject(id_subject)
);

--Tabla borrador del formulario de reserva
CREATE TABLE draft_form(
                           id_draft_form             serial primary key,
                           id_person            	integer not null,
                           id_motive            	integer null,
                           date_reserve_df         date null,
                           number_students_df      integer null,
                           continuous_classroom 	boolean not null,
                           updated_at 		  		timestamp null,
                           created_at  		  	timestamp null,
                           FOREIGN KEY ( id_motive ) REFERENCES motive_reserve ( id_motive ),
                           FOREIGN KEY ( id_person ) REFERENCES person ( id_person )
);

--Tabla formulario de reserva
CREATE TABLE form_reserve(
                             id_form              	serial primary key,
                             id_motive            	integer not null,
                             id_person            	integer not null,
                             date_reserve         	date not null,
                             number_students      	integer not null,
                             continuous_classroom 	boolean not null,
                             updated_at 		  		timestamp not null,
                             created_at  		  	timestamp not null,
                             FOREIGN KEY ( id_motive ) REFERENCES motive_reserve ( id_motive ),
                             FOREIGN KEY ( id_person ) REFERENCES person ( id_person )
);

--Tabla seleccion del o los grupos de una materia o mas
create table reserve_course(
                               id_reserve_course				serial primary key,
                               id_form        					integer not null,
                               id_assign_subject				integer not null,
                               FOREIGN KEY (id_form) REFERENCES form_reserve(id_form),
                               FOREIGN KEY (id_assign_subject) REFERENCES assign_subject(id_assign_subject)
);

--Tabla seleccion borrador del o los grupos de una materia o mas
create table reserve_draf_course(
                                    id_draf_course				serial primary key,
                                    id_draft_form        		integer not null,
                                    id_assign_subject			integer not null,
                                    FOREIGN KEY (id_draft_form) REFERENCES draft_form(id_draft_form),
                                    FOREIGN KEY (id_assign_subject) REFERENCES assign_subject(id_assign_subject)
);


-- Tabla reserva de aula
create table reserve_classroom(
                                  id_reserve         serial primary key,
                                  id_form            integer not null,
                                  notify_reserve     varchar(20) not null,
                                  state_reserve      boolean not null,
                                  date_issued        date not null,
                                  hour_issued        time not null,
                                  updated_at 		  		timestamp not null,
                                  created_at  		  	timestamp not null,
                                  FOREIGN KEY ( id_form ) REFERENCES form_reserve ( id_form )
);

-- Tabla de asignacion de aula
CREATE TABLE assign_classroom(
                                 id_assign_classroom   serial primary key,
                                 id_reserve            integer not null,
                                 id_class_timetable    integer not null,
                                 id_classroom          integer not null,
                                 date_assign_classroom  date not null,
                                 FOREIGN KEY ( id_reserve ) REFERENCES reserve_classroom ( id_reserve ),
                                 FOREIGN KEY ( id_class_timetable ) REFERENCES class_timetable ( id_class_timetable ),
                                 FOREIGN KEY ( id_classroom ) REFERENCES classroom ( id_classroom )
);

--tabla asignacion de periodos de clase
create table assign_class_timetable(
                                       id_assign_class_timetable 	serial primary key,
                                       id_form 					integer not null,
                                       id_class_timetable 			integer not null,
                                       updated_at 		  			timestamp not null,
                                       created_at  		  		timestamp not null,
                                       FOREIGN KEY ( id_form ) REFERENCES form_reserve ( id_form ),
                                       FOREIGN KEY ( id_class_timetable ) REFERENCES class_timetable ( id_class_timetable )
);

--tabla asignacion de periodos de clase
create table assign_draft_class_timetable(
                                             id_assign_draft_class_timetable 	serial primary key,
                                             id_draft_form 						integer not null,
                                             id_class_timetable 					integer not null,
                                             updated_at 		  					timestamp not null,
                                             created_at  		  				timestamp not null,
                                             FOREIGN KEY ( id_draft_form ) REFERENCES draft_form ( id_draft_form ),
                                             FOREIGN KEY ( id_class_timetable ) REFERENCES class_timetable ( id_class_timetable )
);

-- Cambiamos el seteo de fecha para ingresar con el formato dia/mes/año:
set datestyle to 'European';

--subiendo informacion a la DB
insert into role (name_role) values ('Administrador');
insert into role (name_role) values ('Docente');
insert into role (name_role) values ('Auxiliar');

--select * from role;

insert into subject(name_subject) values
                                      ('Ingles I'),
                                      ('Introducción a la programación'),
                                      ('Elem. De programación y estruc. De datos'),
                                      ('Algoritmos avanzados'),('Taller de ingeniería de software');

--select * from subject;

insert into person(id_role, first_name, last_name, email, password_email) values
                                                                              ('1', 'Jorge', 'Mamani Ferrel', 'jorge_mamani@gmail.com', '123abc'),
                                                                              ('2', 'Maria Benita', 'Cespedes Guizada','benita_cespedes@gmail.com ', 'publicaMaria'),
                                                                              ('2', 'Leticia', 'Blanco Coca', 'leticia_blanco@gmail.com', 'publicaLeticia'),
                                                                              ('3', 'Danilo Ernesto', 'Jimenez Guzman', 'danilo_jimenez@gmail.com', 'publicaDanilo'),
                                                                              ('3', 'Edilberto Jonathan', 'Heredia Soliz', 'edilberto_heredia@gmail.com', 'publicaEdilberto');

--select * from  person;

insert into assign_subject(id_person,id_subject,group_number,registered_number) values
                                                                                    ('2', '1', '1', '25'),
                                                                                    ('2', '1', '2', '30'),
                                                                                    ('2', '1', '5', '28'),
                                                                                    ('3', '2', '2', '80'),
                                                                                    ('3', '3', '2', '70'),
                                                                                    ('3', '3', '3', '68'),
                                                                                    ('3', '3', '5', '76'),
                                                                                    ('3', '4', '1', '40'),
                                                                                    ('3', '5', '2', '30'),
                                                                                    ('4', '2', '1', '60'),
                                                                                    ('4', '2', '5', '75'),
                                                                                    ('5', '2', '7', '72'),
                                                                                    ('5', '3', '3', '68');

--select * from assign_subject;

insert into class_timetable(timetable_range) values
                                                 ('06:45 - 08:15'),
                                                 ('08:15 - 09:45'),
                                                 ('09:45 - 11:15'),
                                                 ('11:12 - 12:45'),
                                                 ('12:45 - 14:15'),
                                                 ('14:15 - 15:45'),
                                                 ('15:45 - 17:15'),
                                                 ('17:15 - 18:45'),
                                                 ('18:45 - 20:15'),
                                                 ('20:15 - 21:45');

--select * from class_timetable;

insert into motive_reserve(type_motive) values
                                            ('Toma de examenes'),
                                            ('Clases presenciales');

--select * from motive_reserve;

--Creando tipo ENUM para tipo de aulas

insert into classroom(number_classroom,capacity,type_classroom) values
                                                                    ('622','100','Aula común'),
                                                                    ('623','100','Aula común'),
                                                                    ('624','100','Aula común'),
                                                                    ('690A','30','Aula común'),
                                                                    ('690B','50','Aula común'),
                                                                    ('690C','50','Aula común'),
                                                                    ('690D','50','Aula común'),
                                                                    ('691A','150','Aula común'),
                                                                    ('691B','150','Aula común'),
                                                                    ('691C','90','Aula común'),
                                                                    ('691D','90','Aula común'),
                                                                    ('691E','150','Aula común'),
                                                                    ('691F','150','Aula común'),
                                                                    ('692A','150','Aula común'),
                                                                    ('692B','150','Aula común'),
                                                                    ('692C','90','Aula común'),
                                                                    ('692D','90','Aula común'),
                                                                    ('692E','150','Aula común'),
                                                                    ('692F','150','Aula común'),
                                                                    ('692G','50','Aula común'),
                                                                    ('692H','50','Aula común'),
                                                                    ('693A','150','Aula común'),
                                                                    ('693B','150','Aula común'),
                                                                    ('693C','80','Aula común'),
                                                                    ('693D','80','Aula común'),
                                                                    ('AUDIT 1','300','Auditorio'),
                                                                    ('AUDIT 2','200','Auditorio');

